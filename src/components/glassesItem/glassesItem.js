module.exports = {
  template: require('./glassesItem.html'),
  controller: ['cartService', ctrl],
  bindings: {
    item: '='
  }
};

function ctrl(cartService) {
  var self = this;

  self.addToCart = addToCart;

  function addToCart() {
    cartService.addToCart(self.item);
  }
}
