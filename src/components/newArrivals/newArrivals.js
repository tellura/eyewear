module.exports = {
  template: require('./newArrivals.html'),
  controller: ['glassesService', ctrl]
};

function ctrl(glassesService) {
  var self = this;

  self.newArrivals = [];
  self.showLoadMore = true;

  self.loadMore = loadMore;

  glassesService.getNewArrivals(4, 0)
    .then(function (data) {
      self.newArrivals = data;
    });

  function loadMore() {
    glassesService.getNewArrivals(8, self.newArrivals.length)
      .then(function (data) {
        self.newArrivals = self.newArrivals.concat(data);
        if (data.length < 8) {
          self.showLoadMore = false;
        }
      });
  }
}
