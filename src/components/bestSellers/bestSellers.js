module.exports = {
  template: require('./bestSellers.html'),
  controller: ['glassesService', ctrl]
};

function ctrl(glassesService) {
  var self = this;

  self.bestSellers = [];
  self.showLoadMore = true;

  self.loadBestSellers = loadBestSellers;

  glassesService.getBestSellers(4, 0)
    .then(function (data) {
      self.bestSellers = data;
    });

  function loadBestSellers() {
    glassesService.getBestSellers(8, self.bestSellers.length)
      .then(function (data) {
        self.bestSellers = self.bestSellers.concat(data);
        if (data.length < 8) {
          self.showLoadMore = false;
        }
      });
  }
}
