module.exports = {
  template: require('./orderPage.html'),
  controller: ['cartService', ctrl]
};

function ctrl(cartService) {
  var self = this;

  self.cart = cartService.getCart();

  self.getTotal = getTotal;

  function getTotal() {
    return cartService.getTotal();
  }
}
