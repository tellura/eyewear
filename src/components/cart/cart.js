module.exports = {
  template: require('./cart.html'),
  controller: ['cartService', ctrl]
};

function ctrl(cartService) {
  var self = this;

  self.isOpen = false;

  self.openCart = openCart;
  self.closeCart = closeCart;
  self.clearCart = clearCart;
  self.getTotal = getTotal;

  function openCart() {
    self.isOpen = true;
    self.cart = cartService.getCart();
  }

  function closeCart() {
    self.isOpen = false;
  }

  function clearCart() {
    self.cart = cartService.clearCart();
  }

  function getTotal() {
    return cartService.getTotal();
  }
}
