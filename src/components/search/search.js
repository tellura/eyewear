module.exports = {
  template: require('./search.html'),
  controller: [ctrl]
};

function ctrl() {
  var self = this;

  self.isOpen = false;

  self.showInput = showInput;

  function showInput() {
    self.isOpen = true;
  }
}
