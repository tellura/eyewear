module.exports = {
  template: require('./brands.html'),
  controller: ['brandService', ctrl]
};

function ctrl(brandService) {
  var self = this;

  self.brands = brandService.getBrands();
}
