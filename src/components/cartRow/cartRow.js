module.exports = {
  template: require('./cartRow.html'),
  controller: ['cartService', ctrl],
  bindings: {
    item: '='
  }
};

function ctrl(cartService) {
  var self = this;

  self.increase = increase;
  self.decrease = decrease;
  self.removeFromCart = removeFromCart;

  function increase() {
    cartService.addToCart(self.item);
  }

  function decrease() {
    cartService.decrease(self.item);
  }

  function removeFromCart() {
    cartService.removeFromCart(self.item);
  }
}
