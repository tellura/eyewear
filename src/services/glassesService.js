module.exports = ['bestSellersConstant', 'newArrivalsConstant', '$timeout', glassesService];

function glassesService(bestSellersConstant, newArrivalsConstant, $timeout) {
  var api = {
    getBestSellers,
    getNewArrivals
  };

  function getBestSellers(take, skip) {
    return $timeout(function () {
      return bestSellersConstant.slice(skip, skip + take);
    }, 500);
  }

  function getNewArrivals(take, skip) {
    return $timeout(function () {
      return newArrivalsConstant.slice(skip, skip + take);
    }, 500);
  }

  return api;
}
