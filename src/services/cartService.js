module.exports = [cartService];

function cartService() {
  var api = {
    addToCart,
    getCart,
    clearCart,
    getTotal,
    decrease,
    removeFromCart
  };
  var cart = [];

  function addToCart(item) {
    var equals = cart.filter(function (el) {
      return el.id === item.id;
    });

    if (equals.length) {
      equals[0].count++;
      equals[0].amount = equals[0].price * equals[0].count;
    } else {
      cart.push({
        name: item.name,
        oldPrice: item.oldPrice,
        price: item.price,
        img: item.img,
        id: item.id,
        count: 1,
        amount: item.price
      });
    }
  }

  function getCart() {
    return cart;
  }

  function clearCart() {
    cart.length = 0;
  }

  function getTotal() {
    var total = 0;

    cart.forEach(function (item) {
      total += item.amount;
    });

    return total;
  }

  function decrease(item) {
    for (var i = 0; i < cart.length; i++) {
      if (item.id === cart[i].id) {
        if (cart[i].count > 1) {
          cart[i].count--;
          cart[i].amount = cart[i].price * cart[i].count;
        } else {
          cart.splice(i, 1);
        }
      }
    }
  }

  function removeFromCart(item) {
    for (var i = 0; i < cart.length; i++) {
      if (item.id === cart[i].id) {
        cart.splice(i, 1);
      }
    }
  }

  return api;
}
