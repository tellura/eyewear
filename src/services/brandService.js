module.exports = ['brandsConstant', brandService];

function brandService(brandsConstant) {
  var api = {
    getBrands
  };

  function getBrands() {
    return brandsConstant;
  }

  return api;
}
