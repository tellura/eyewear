module.exports = routesConfig;

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('mainPage', {
      url: '/',
      component: 'mainPage'
    })
    .state('order', {
      url: '/order',
      component: 'orderPage'
    });
}
