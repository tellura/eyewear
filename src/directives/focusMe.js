module.exports = ['$timeout', '$parse', focusMe];

function focusMe($timeout, $parse) {
  return {
    link: function (scope, element, attrs) {
      var model = $parse(attrs.focusMe);
      scope.$watch(model, watchFn);
      element.bind('blur', blurFn);

      function watchFn(value) {
        if (value === true) {
          $timeout(function () {
            element[0].focus();
          });
        }
      }

      function blurFn() {
        scope.$apply(model.assign(scope, false));
      }
    }
  };
}
