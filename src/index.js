var angular = require('angular');
require('angular-ui-router');
var routesConfig = require('./routes');

require('./index.scss');
require('../node_modules/normalize.css/normalize.css');

var glassesService = require('./services/glassesService');
var cartService = require('./services/cartService');
var brandService = require('./services/brandService');
var focusMe = require('./directives/focusMe');
var bestSellersConstant = require('./constants/bestSellersConstant');
var newArrivalsConstant = require('./constants/newArrivalsConstant');
var brandsConstant = require('./constants/brandsConstant');
var mainPage = require('./components/mainPage/mainPage');
var navBar = require('./components/navBar/navBar');
var search = require('./components/search/search');
var cart = require('./components/cart/cart');
var cartRow = require('./components/cartRow/cartRow');
var glassesContainer = require('./components/glassesContainer/glassesContainer');
var specialOffers = require('./components/specialOffers/specialOffers');
var bestSellers = require('./components/bestSellers/bestSellers');
var glassesItem = require('./components/glassesItem/glassesItem');
var newArrivals = require('./components/newArrivals/newArrivals');
var brand = require('./components/brand/brand');
var brands = require('./components/brands/brands');
var newsLetter = require('./components/newsLetter/newsLetter');
var bottom = require('./components/bottom/bottom');
var orderPage = require('./components/orderPage/orderPage');

var app = 'app';
module.exports = app;

angular
  .module(app, ['ui.router'])
  .factory('glassesService', glassesService)
  .factory('cartService', cartService)
  .factory('brandService', brandService)
  .config(routesConfig)
  .directive('focusMe', focusMe)
  .constant('bestSellersConstant', bestSellersConstant)
  .constant('newArrivalsConstant', newArrivalsConstant)
  .constant('brandsConstant', brandsConstant)
  .component('mainPage', mainPage)
  .component('navBar', navBar)
  .component('search', search)
  .component('cart', cart)
  .component('cartRow', cartRow)
  .component('glassesContainer', glassesContainer)
  .component('specialOffers', specialOffers)
  .component('bestSellers', bestSellers)
  .component('glassesItem', glassesItem)
  .component('newArrivals', newArrivals)
  .component('brand', brand)
  .component('brands', brands)
  .component('newsLetter', newsLetter)
  .component('bottom', bottom)
  .component('orderPage', orderPage);
